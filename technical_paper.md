# Browser Rendering
### Rendering :
 Rendering in web browsers means converting the code (HTML, CSS, and JavaScript) of a webpage into what you see on your screen. The browser first reads the HTML to understand the structure of the page, then applies CSS to make it look good, and finally runs any JavaScript for interactivity. The end result is what you view in your browser window.
 
 ### Content:
  * Overview
  * How does a browser render HTML, CSS, JS to DOM ?
       * What is the Render Tree?
       * What is the DOM Tree?
       * How the browser renders a web page ?
  * What is the mechanism behind it ?
  * Reference 


### Overview:
In this article, we're going to take a closer look at two important things: the DOM and the CSSOM. These play crucial roles in how your web browser displays a webpage. Sometimes, your browser waits for certain things to load before showing you a webpage. Other times, it loads things at the same time. When you're building a website, you want it to load quickly and look good from the start.

But sometimes, it might load slowly, show weird stuff for a moment, or just not look right. We can avoid these issues by understanding how a browser typically puts together a webpage. First, let's talk about the DOM. When you open a webpage, your browser asks a server for an HTML document. The server sends back the HTML file, which is like a text document with special instructions for the browser.

The browser knows it's HTML because of a thing called the MIME Type. This just tells the browser what kind of document it is. Then there's something called charset=UTF-8, which tells the browser how the text is written. This helps the browser read the HTML correctly.

### How does a browser render HTML,CSS Js to DOM ?
#### What is the Render Tree ?
User
The render tree is like a map that tells the web browser what to display on the screen when you open a webpage. It's made by combining two sets of instructions: one that defines the structure of the webpage (called the DOM) and another that specifies how things should look (called the CSSOM).

Once the render tree is made, the browser knows exactly which parts of the webpage should be visible and how they should look. It then organizes everything on the screen according to this plan, making sure it all looks good.

So, in simple terms, the render tree is like a plan that the browser follows to show you the webpage correctly. It includes everything you can see on the screen and how it should appear, based on the webpage's design instructions. If something is supposed to be hidden, it won't be included in this plan. If an element is hidden using CSS (for example, with display: none), it's still there in the webpage's structure (the DOM), but it won't appear in the render tree because it's not meant to be seen.

When the browser builds the render tree, it goes through the visible parts of the webpage, starting from the main structure (the DOM). Elements like ``<script> or <meta> ``tags are skipped because they don't affect what you see on the screen. And anything hidden by CSS isn't included because it's not supposed to be shown.    convert it in to simple english

![Alt text](https://miro.medium.com/v2/resize:fit:720/format:webp/1*FXGPMAFT3lXjMupFQOY4jg.png)

#### What is the DOM Tree?
A DOM tree is a type of tree whose nodes represent the content of the HTML and XML document. Every HTML or XML document has a unique DOM tree representation. For example, let’s examine the following code:
``` html
<html>
<head>
  <title>My document</title>
</head>
<body>
  <h1>Header</h1>
  <p>Paragraph</p>
</body>
</html>
```

The DOM tree of the above code block is as follows:

![Alt text](https://miro.medium.com/v2/resize:fit:720/format:webp/1*iiNCB2zkF5a6mFMZZR5hgQ.png)


#### How the browser renders a web page ?
#### 1. Parse the HTML

  When the browser begins to receive the HTML data of a page over the network, it immediately sets its parser to work to convert the HTML into a Document Object Model (DOM).

The first step of this parsing process is to break down the HTML into tokens that represent start tags, end tags, and their contents. From that it can construct the DOM.
![Alt text](https://starkie.dev/images/blog/how-a-browser-renders-a-web-page/step-1.webp)

#### 2. Fetch external resources
When the web browser reads a webpage, it comes across things like CSS and JavaScript files that are stored separately from the main HTML file. To get these files, the browser goes and fetches them. While it's waiting for a CSS file to load, it keeps reading the HTML but doesn't show anything on the screen until the CSS file is fully loaded and understood.

JavaScript files work a bit differently. By default, they stop the browser from reading the rest of the HTML until they're fully loaded and understood. But there are two tricks to make things smoother: "defer" and "async". Both let the browser keep reading the HTML while the JavaScript file loads in the background.

"defer" means the JavaScript code will wait until the whole HTML page is done being read before it runs. If there are multiple "defer" scripts, they run in the order they were found in the HTML.

``` html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Example Page</title>
    <!-- External CSS file -->
    <link rel="stylesheet" href="styles.css">
</head>
<body>
    <h1>Hello, World!</h1>
    <p>This is a simple example webpage.</p>

    <!-- JavaScript files with different attributes -->
    <script src="script1.js" defer></script>
    <script src="script2.js" async></script>
</body>
</html>
```
* In our webpage, we use a separate file called ``styles.css`` to make the page look nice with colors, fonts, and layouts.

* We also have two JavaScript files called ``script1.js and script2.js``. These files make our webpage interactive and dynamic.

* When the browser reads our webpage, it sees these JavaScript files. For ``script1.js``, we tell the browser to load it in the background while it reads the webpage. But it won't run the JavaScript until it's done reading everything.

* For ``script2.js``, we tell the browser to also load it in the background while reading the webpage. But this time, we also tell it to go ahead and run the JavaScript as soon as it's ready, even if it hasn't finished reading everything.
* These tricks help make our webpage load faster and appear on your screen more quickly, because the browser doesn't have to wait for the JavaScript files to finish loading before showing you the webpage.
  
#### 3. Parse the CSS and build the CSSOM
You may well have heard of the DOM before, but have you heard of the CSSOM (CSS Object Model)? Before I started researching this topic a little while back, I hadn't!
Similar to HTML files and the DOM, when CSS files are loaded they must be parsed and converted to a tree - this time the CSSOM. It describes all of the CSS selectors on the page, their hierarchy and their properties.

Where the CSSOM differs to the DOM is that it cannot be built incrementally, as CSS rules can overwrite each other at various different points due to specificity. This is why CSS blocks rendering, as until all CSS is parsed and the CSSOM built, the browser can't know where and how to position each element on the screen.


![Alt type](https://starkie.dev/images/blog/how-a-browser-renders-a-web-page/step-3.webp)

#### 4. Execute the JavaScript
Once all the parts of the webpage are loaded, including HTML, CSS, and JavaScript, the browser starts running the JavaScript code. This involves understanding the JavaScript code, preparing it for the computer to use, and then making it work. Each browser has its own way of doing this job.

``` javascript
//sample  code
function greet() {
    console.log("Hello, world!");
}

greet(); // Calling the  function
```
In this example, we have a simple function called greet that prints "Hello, world!" to the console. When the browser gets to this part of the code, it runs the greet function and shows "Hello, world!" in the console.

Additionally, there's an important event called DOMContentLoaded. This event happens when the HTML document and all the JavaScript that needs to be loaded with it are fully ready to use. It's a good idea to wait for this event before doing anything with the webpage.

``` java script
document.addEventListener('DOMContentLoaded', function() {
    console.log("The webpage is fully loaded.");
});
```
In this code, we're saying that when the DOMContentLoaded event happens (meaning the HTML and all the synchronous JavaScript are ready), run the function inside the event listener. This ensures that any JavaScript code that needs to change the webpage or listen for user actions will only run after the webpage is fully loaded and ready to be used.

![Alt type](https://starkie.dev/images/blog/how-a-browser-renders-a-web-page/step-4.webp)

#### 5. Merge DOM and CSSOM to construct the render tree 
Once the browser has both the DOM (webpage structure) and CSSOM (applied styles), it merges them to form the render tree. This render tree includes everything to be shown, but not all nodes are necessarily visible. For instance, elements with "opacity: 0" or "visibility: hidden" may still be included but not seen. However, elements with "display: none" won't be included. Additionally, non-visual elements like the ```<head>``` tag are always omitted. Different browsers have different rendering engines to convert the render tree into the visible webpage.

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Render Tree Example</title>
    <style>
        /* CSS styles */
        .visible {
            opacity: 1; /* visible */
        }
        .invisible {
            visibility: hidden;  tree */
        }
        .hidden {
            display: none;
        }
    </style>
</head>
<body>
    <div id="visible" class="visible">Visible content</div>
    <div id="invisible" class="invisible">Invisible content</div>
    <div id="hidden" class="hidden">Hidden content</div>
</body>
</html>


```
* We define three <div> elements with classes: "visible", "invisible", and "hidden".
* "visible" sets opacity to 1, making it visible.
* "invisible" sets visibility to hidden, still included in render tree but not visible.
* "hidden" sets display to none, excluded from render tree and not visible.
  
#### 6. Calculate layout and paint
Now that we have a complete render tree the browser knows what to render, but not where to render it. Therefore the layout of the page (i.e. every node's position and size) must be calculated. The rendering engine traverses the render tree, starting at the top and working down, calculating the coordinates at which each node should be displayed.

Once that is complete, the final step is to take that layout information and paint the pixels to the screen.

And voila! After all that, we have a fully rendered web page!


![Altn type](https://starkie.dev/images/blog/how-a-browser-renders-a-web-page/step-6.webp)


### References:

* https://developer.mozilla.org/en-US/docs/Web/API/Document_Object_Model/Introduction
  
* https://developer.mozilla.org/en-US/docs/Web/API/CSS_Object_Model
* https://en.wikipedia.org/wiki/Comparison_of_browser_engines
* https://starkie.dev/blog/how-a-browser-renders-a-web-page
* https://developer.mozilla.org/en-US/docs/Web/Performance/How_browsers_work

* https://www.w3schools.com/js/js_htmldom.asp