### 1. What are the steps/strategies to do Active Listening?
1. __Fully Hear and Comprehend:__
   * Fully listening and understanding involves focusing on comprehending the message conveyed by the speaker.
2. __Avoid Distractions:__
   * Remain mentally focused and attentive, steering clear of internal distractions.
3. __Resist Interrupting:__
   * Allow the speaker to complete their thoughts before providing a response.
4. __Show Interest with Body Language:__
   * Utilize non-verbal cues such as nodding and maintaining eye contact to show understanding and engagement with the speaker.
5. __Take Notes and Paraphrase:__
   * Capture key points in notes and paraphrase to confirm understanding.

### 2.According to Fisher's model, what are the key points of Reflective Listening?

* When you talk to someone, focus completely on them. Try to get rid of any distractions so you can really listen.
*  Imagine things from their perspective, even if you don't agree. Be open-minded and caring, so they feel comfortable sharing.
*  Pick up on their mood not just from words but how they say things. Listen to their tone, body language, and expressions. Match their feelings.
*  After they speak, tell them what you understood using your own words. It's different from just repeating; you're sharing what you got from their message.


### 3. What are the obstacles in your listening process?

__1. Mitigating Distractions:__

* Choose a quiet and focused environment for conversations.
* Minimize potential interruptions, such as silencing electronic devices.
* Practice active listening techniques to stay fully engaged with the speaker.*



__2. Addressing Mind Wandering:__

* Develop mindfulness techniques to stay present during discussions.
* Set aside personal concerns before entering conversations.
* Practice mental exercises to improve concentration and attention.



__3. Cultivating Patience:__

* Remind yourself to listen attentively without rushing to respond.
* Allow natural pauses in the conversation for the speaker to express themselves fully.
* Practice active listening as a form of patience, letting the speaker have their say.

__4. Clarifying Information:__

* Encourage a culture of open communication, where asking for clarification is welcomed.
* Summarize key points during the conversation to ensure mutual understanding.
* Be proactive in seeking additional information when faced with ambiguity.

### 5. When do you switch to Passive communication style in your day to day life?

* When the primary goal is to avoid conflict or maintain a peaceful atmosphere, I opt for a passive communication style to prevent potential disagreements.
* When I lack confidence or fear negative reactions I choose a passive style to avoid drawing attention to myslef or my opinions.
* When I am concerned about criticism or rejection, I use a passive style to minimize the chance of receiving negative feedback.
* In group settings or collaborative environments, I adopt a passive style to prevent disruptions or maintain a smooth flow of interactions.
* When interacting with figures of authority, I prefer a passive communication style out of respect or deference.
  
### 6. When do you switch into Aggressive communication styles in your day to day life?
* When I am super mad, I use aggressive communication to show just how upset I are.
* If I feel like I am in danger, I act aggressively to protect myself.
* I sometimes use aggression to try and control a situation or make sure others listen to me.
* When I feel like my personal space is invaded, I react aggressively to set boundaries.
  
### 7. When do you switch into Passive Aggressive communication styles in your day to day life?
* Learn to recognize and name your needs. Dig deeper to understand the underlying needs behind your requests. For instance, if you need a day off work, consider whether it's due to burnout and a desire for a more sustainable work schedule.
* Practice assertive communication in low-stakes situations to build confidence. Begin with safe people and easy scenarios before tackling more challenging or high-stakes conversations, such as those with authority figures.
* Pay attention to your body language and tone of voice. Ensure that your non-verbal cues align with your assertive communication. Being aware of your body can help you regulate emotions and prevent extreme reactions in challenging situations.
* Address problematic situations early on. Speak up about issues that bother you as soon as possible to prevent them from becoming recurring problems. Waiting to address mistreatment or unmet needs may lead to the behavior continuing or worsening.