####  What kinds of behaviour cause sexual  harassment?

1. Unwanted physical contact such as touching, groping, or hugging.
2. Making sexual advances or propositions.
3. Making suggestive comments, jokes, or gestures.
4. Displaying sexually explicit materials such as photos, videos, or drawings.
5. Sending sexually explicit emails, texts, or messages.
6. Making unwanted sexual inquiries or comments about someone's appearance or clothing.
7. Making unwanted sexual remarks about someone's body or attractiveness.
8. Persistent or unwanted requests for dates or sexual favors.
9. Staring, leering, or making sexually suggestive gestures.
10. Making sexual innuendos or using sexualized language.
11. Spreading rumors or gossip of a sexual nature about someone.
12. Making offensive remarks about someone's gender, sexual orientation, or identity.
13. Using power or authority to coerce or intimidate someone into sexual activity or compliance.
14. Retaliating against someone who rejects or reports sexual advances or harassment.
15. Creating a hostile or intimidating environment through sexually charged language, behavior, or imagery.
#### What would you do in case you face or witness any incident or repeated incidents of such behaviour?

1. **Assess the Situation**: Evaluate the severity of the behavior and whether it constitutes sexual harassment or misconduct.

2. **Document**: Keep detailed records of the incidents, including dates, times, locations, and descriptions of what occurred. Document any witnesses present.

3. **Speak Up**: If it is safe to do so, confront the perpetrator directly and clearly communicate that their behavior is unwelcome and inappropriate. If witnessing the behavior, intervene to stop it if possible.

4. **Report**: Report the incidents to the appropriate authorities or individuals, such as human resources, a supervisor, school administration, or law enforcement, depending on the context in which the harassment occurred. Follow the organization's procedures for reporting sexual harassment.

5. **Seek Support**: Seek support from trusted friends, family members, or colleagues. Consider reaching out to support services or counseling for assistance in coping with the experience.

6. **Know Your Rights**: Familiarize yourself with your rights and protections against sexual harassment under applicable laws and organizational policies. Seek legal advice if necessary.
