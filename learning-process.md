# 1. How to Learn Faster with the Feynman Technique

### Question 1: 

#### What is the Feynman Technique? Explain in 1 line.

* It is a learning process designed to make us learn things in an efficient way.

# 2. Learning How to Learn TED talk by Barbara Oakley

### Question 2:
#### In this video, what was the most interesting story or idea for you?
* Pick a topic and study
* Explain it to someone unfamiliar with the topic in a simple language
* Identify issues in your understanding
* Study the topic again to clarify your issues in your understanding
* Repeat from step 2

### Question 3:
#### What are active and diffused modes of thinking?

  There are two modes in our brain.

* Focus Mode
* Diffuse Mode

Pomodoro Technique

* Set a timer for 25 minutes
* Turn off all the distractions
* Work in Focused Mode for 25 minutes
* After 25 minutes of work, do something fun for few minutes
* This helps with practicing both focused mode of working and relaxation

# 3. Learn Anything in 20 hours
### Question 4:
#### According to the video, what are the steps to take when approaching a new topic? Only mention the points.

* Don't get distracted
* Practice daily
* Test yourself all the time
* Practice topics until the solution plays from your mind like a song
* Look at a page, look away, see what you can recall

### Question 5:


#### What are some of the actions you can take going forward to improve your learning process?

* Learn enough to self-correct
* Remove distractions during practice
* Practice consistently, everyday.
* Practice at least 45 minutes a day.