### Question 1
#### Paraphrase (summarize) the video in a few lines. Use your own words.
* Paraphrase (summarize) the video in a few lines. Use your own words.
  Here is Grit by Angela Duck worth, which is a hope for them. The video is about zeal and hard work in achievement of goal. The author in her research on children found that the I.Q. a gold standard for prediction of intellectual excellence falls short when one works with passion and persistence.

### Question 2
#### Paraphrase (summarize) the video in a few (1 or 2) lines in your own words.
* To develop problem-solving skills, believe in your ability to grow and learn. Some believe talents are fixed, while others with a growth mindset see challenges as opportunities. Challenge assumptions, embrace new ideas, and learn from struggles to grow amidst chaos.

### Question 3
#### What is the Internal Locus of Control? What is the key point in the video?
* People with an internal locus of control believe they can control what happens in their lives, which helps them stay motivated. Locus of Control is about how much we think we can control our lives, with two types: external and internal. We have control over how much effort we put into things. To develop an internal locus of control, we can solve problems in our lives and feel good about our efforts.
  
### Question 4
#### What are the key points mentioned by speaker to build growth mindset (explanation not needed).
*  Lot of people have whats's called a fixed mindset which is there they dont believe they can grow,They don't believe they can get better, they dont believe  there personality or there certain levels and talent was kind of god-given and they cannot bust throught.
* Growth Mindset is the foundation for learning
* Focus should be on process of getting better
* Effort should be put in to get better
* Don't back down or avoid
* Accept feedbacks
* Appreciate feedbacks


### Question 5
#### What are your ideas to take action and build Growth Mindset?
* Take full responsibility for your learning journey and commit to staying with a problem until it's solved, leveraging the understanding that increased effort leads to better comprehension.

* Prioritize thorough understanding of each concept and treat new challenges as learning opportunities, remaining calm and focused even in challenging situations.

* Utilize available resources such as documentation, Google, Stack Overflow, and GitHub before seeking help, demonstrating self-reliance and ownership of your projects.
  
* Embrace confusion, discomfort, and errors as integral parts of the learning process, recognizing them as signals for deeper understanding and improvement.

* Maintain a confident and enthusiastic demeanor, approaching challenges with a positive attitude and serving users and society with high-quality, reliable software solutions.